
import java.util.Scanner;
public class caesarcipher
{
    public static void main(String[] args)
    {
Scanner scan = new Scanner(System.in);
    String ciph;
    String key;
    int keyLength;

    System.out.println("Enter message:");
    ciph = scan.nextLine();
    System.out.println("Enter encryption key:");
    key = scan.next();
    keyLength=key.length();
      for(;;)
    {
        System.out.println("1.Encrypt\n2.Decrypt\n3.Exit...");
        int choice=scan.nextInt();
        switch(choice)
        {
            case 1:

                System.out.println("Encrypted message..."+encrypt(ciph,keyLength));
                break;
            case 2:

                System.out.println("Decryptedmessage..."+decrypt(encrypt(ciph,keyLength),keyLength));
                break;
            case 3:
                               System.exit(0);
                break;
            default:
            System.out.println("Invalid option..");
        }
    }
}
public static String encrypt(String ciph,int keyLength)
{
    String encrypted="";
    for(int i=0;i<ciph.length();i++)
    {
                int c=ciph.charAt(i);

        if(Character.isUpperCase(c))
        {
            c=c+(keyLength%26);
                      if(c >'Z')
                c=c-26;
        }
               else if(Character.isLowerCase(c))
        {
            c=c+(keyLength%26);

            if(c>'z')
                c=c-26;
        }

        encrypted=encrypted+(char) c;
    }
    return encrypted;
}
public static String decrypt(String ciph,int keyLength)
{
    String decrypted="";
    for(int i=0;i<ciph.length();i++)
    {
                int c=ciph.charAt(i);

        if(Character.isUpperCase(c))
        {
            c=c-(keyLength%26);

            if(c<'A')
                c=c+26;
        }
                else if(Character.isLowerCase(c))
        {
            c=c-(keyLength%26);

            if(c<'a')
                c=c+26;
        }

        decrypted=decrypted+(char) c;
    }
    return decrypted;
}
}
