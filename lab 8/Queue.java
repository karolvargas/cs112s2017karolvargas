import java.util.Arrays;

public class Queue<T> {

    T[] queue;
    int size;

    public int front;
    public int rear;

    public T remove() {
    T value = null;
    if (isempty()) {
        throw new IllegalStateException("Empty Queue");
    } else if (front == rear) {
        value = queue[front];
        front = -1;
        rear = -1;

    } else {
        value = queue[front];
        front=(front+1)%size;

    }
    return value;
   }




public T peek(){
  T value = null;
  if (isempty())
  {
      throw new IllegalStateException("Empty Queue");
    }// if
  else
  {
    value = queue[front];
  }// else
return value;
}




    public Queue(int inSize) {
        size = inSize;
        queue = (T[]) new Object[size];
        front = -1;
        rear = -1;
    }



       public boolean isempty() {
           return (front == -1 && rear == -1);
       }



       public void add(T value) {
           if ((rear+1)%size==front) {
               throw new IllegalStateException("Queue is full");

           } else if (isempty()) {
               front++;
               rear++;
               queue[rear] = value;

           } else {
               rear=(rear+1)%size;
               queue[rear] = value;

           }
       }



    @Override
    public String toString() {
        return "Queue [front=" + front + ", rear=" + rear + ", size=" + size
                + ", queue=" + Arrays.toString(queue) + "]";
    }

}

