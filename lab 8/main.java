import java.io.*;
import java.util.Stack;

public class main
{
    public static void main( String[] args)
    {
        Stack<Integer> stack = new Stack<Integer>(10);

       try{ for(int i = 1; i < 10; i++){
            stack.pop();
        }}
        catch(EmptyStackException e1){
            throw new EmptyStackException("Stack was empty!"+e1);
        }
    }
}
