public class Stringlab5
{
    public static void main(String args[])
    {
        long startTime = System.nanoTime();

        String myString = new String();
        for(int i = 0; i < 100000; i++){

            myString = myString+"L";
        }

       long endTime = System.nanoTime();

        long runTime = endTime - startTime;
        System.out.println("new string:"+myString);
        System.out.println("run time:"+runTime);
    }
}
