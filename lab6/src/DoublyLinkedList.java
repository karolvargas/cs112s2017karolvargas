class DoublyLinkedList {

    private static class Node {

        protected int data;
        protected Node next, previous;

        public Node() {
            next = null;
            data = 0;
            previous = null;
        } //Node (constructor)

        public Node(int d, Node n, Node p) {
            next = n;
            data = d;
            previous = p;
        } //Node (constructor)

    } //Node (class)



    // DoublyLinkedList stuff starts here
    private int size;
    private Node head, tail;

    public DoublyLinkedList() {
        head = null;
        tail = null;
        size = 0;
    } //DoublyLinkedList (constructor)

    public int size() {
        return size;
    } //size

    public boolean isEmpty() {
        return (size == 0);
    } //isEmpty

    public void add(int newInt) {

        Node qwer = new Node(newInt, null, null);
        if(size == 0){
            tail = qwer;
            head = qwer;
            size++;
            return ;
        }

        tail.next = qwer;
        qwer.previous = tail;
        tail = qwer;
        size++;



    } //add

    public int get(int index) {
        Node currentPosition = head;
        int counter = 0;

        // check to make sure the index is less than size
        if (index >= size || index < 0) {
            System.out.println("Hey you gave me bad input -- get");
            return -1;
        } //if

        while (counter < index) {
            System.out.print(counter+"and"+index);
            currentPosition = currentPosition.next;
            counter++;
        } //while

        return currentPosition.data;
    } //get

    public int getFromEnd(int index) {
        Node Endposition = tail;
        int asdf = size;

        if (index >= size || index < 0) {
            System.out.println("Hey you gave me bad input -- get");
            return -1;
        } //if

        while( asdf > index ){
            Endposition = Endposition.previous;
            asdf--;

        }

        return Endposition.data;





    } //getFromEnd

    public void remove(int index) {

        if(index <= -1 || index >= size ){
            System.out.println("Hey you gave me a bad input! ");
        }
        else if(0 == index){
           if (1 == size){
                tail = null;
                head = null;
            }

            else{

            Node t = head.next;
            head.next = null;
            head = t;

            head.previous = null;


            }
        }
        else if(index == (size-1)){
            tail = tail.previous;
            tail.next.previous = null;
            tail.next = null;
        }
        else{
            Node currentPosition = head;

            int counter = 0;
            while(counter < index){
                currentPosition = currentPosition.next;
                counter++;
            }
            currentPosition.previous.next = currentPosition.next;
            currentPosition.next.previous = currentPosition.previous;
            currentPosition.next = null;
            currentPosition.previous = null;
        }
        size--;


    }//remove

    public boolean testPreviousLinks() {
        Node current = tail;
        int count = 1;
        while (current.previous != null) {
            current = current.previous;
            count++;
        } //while
        return ((current == head) && (count == size));
    }
} //testPreviousLinks

//DoublyLinkedList (class)




