import java.util.Scanner;
import java.util.Stack;

import java.util.*;


public class PostfixCalculator
{
public static void main (String[] args)
{

Scanner scan = new Scanner(System.in);
System.out.println("Enter postfix:");
String conversion = scan.nextLine();
Stack<Integer> stack = new Stack<Integer>();


for(int z = 0; z < conversion.length(); z++){

    int a = 0;
    int b = 0;
    int result = 0;
    if(Character.isDigit(conversion.charAt(z))){
       int convertString = Character.getNumericValue(conversion.charAt(z));
        stack.push(convertString);
    }

    else if(conversion.charAt(z) == '+')
    {
      if(!(stack.isEmpty()))
      {
        a = stack.pop();
        b = stack.pop();
        result = a+b;
        stack.push(result);
      }
    }
     else if(conversion.charAt(z) == '-')
     {
        if(!(stack.isEmpty()))
        {
        a = stack.pop();
        b = stack.pop();
        result = a-b;
        stack.push(result);
      }
    }
     else if(conversion.charAt(z) == '*')
     {
        if(!(stack.isEmpty()))
        {
        a = stack.pop();
        b = stack.pop();
        result = a*b;
        stack.push(result);
      }
    }
     else
     {
       if(!(stack.isEmpty()))
       {
        a = stack.pop();
        b = stack.pop();
        result = a/b;
        stack.push(result);
      }
    }
}

int calculatedValue = stack.pop();
System.out.println(calculatedValue);
 }
 }
